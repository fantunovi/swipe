package com.example.frane.swipe;


import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Frane on 16.3.2015..
 */
public class PrikazPodatakaFragment extends Fragment {

    SQLiteHelper db;
    SwipeRefreshLayout mSwipeRefreshLayout;
    ListView listView;
    SupportMapFragment fragment;
    GoogleMap map;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_prikaz, container, false);

        listView = (ListView) rootView.findViewById(R.id.listView);
        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_refresh_layout);

        MapsInitializer.initialize(getActivity());

        switch (GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity())) {
            case ConnectionResult.SUCCESS:
                Toast.makeText(getActivity(), "Uspjeh!", Toast.LENGTH_SHORT).show();
                FragmentManager fm = getChildFragmentManager();
                fragment = (SupportMapFragment) fm.findFragmentById(R.id.map);
                map = fragment.getMap();
                if (map != null) {
                    map.getUiSettings().setMyLocationButtonEnabled(false);
                    map.setMyLocationEnabled(true);
                    CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(45.8167, 15.9833), 10);
                    map.animateCamera(cameraUpdate);
                }
                break;
            case ConnectionResult.SERVICE_MISSING:
                Toast.makeText(getActivity(), "Servis je nedostupan!", Toast.LENGTH_SHORT).show();
                break;
            case ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED:
                Toast.makeText(getActivity(), "Potrebna nadogradnja!", Toast.LENGTH_SHORT).show();
                break;
            default:
                Toast.makeText(getActivity(), GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity()), Toast.LENGTH_SHORT).show();
        }


        FragmentManager fm = getChildFragmentManager();
        fragment = (SupportMapFragment) fm.findFragmentById(R.id.map);
        map = fragment.getMap();

        refreshContent();

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshContent();
            }
        });
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        FragmentManager fm = getChildFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.remove(fragment);
        ft.commitAllowingStateLoss();
    }

    private void refreshContent() {
        db = new SQLiteHelper(getActivity());

        final List<Osoba> values = db.izlistajOsobe();
        List<Map<String, String>> data1 = new ArrayList<Map<String, String>>();

        for (int i = 0; i < values.size(); i++) {
            HashMap<String, String> osoba = new HashMap<String, String>();
            osoba.put("item", values.get(i).getName().toString() + " " + values.get(i).getSurname().toString() + ", " + values.get(i).getOib().toString());
            osoba.put("subitem", values.get(i).getAddress().toString());
            data1.add(osoba);
        }

        SimpleAdapter adapter1 = new SimpleAdapter(getActivity(), data1, android.R.layout.simple_list_item_2, new String[]{"item", "subitem"}, new int[]{android.R.id.text1, android.R.id.text2});

        listView.setAdapter(adapter1);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String add = values.get(position).getAddress();
                Address address = getLocationFromAddress(add);
                double lat = address.getLatitude();
                double lon = address.getLongitude();
                if (map == null) {
                    Toast.makeText(getActivity(), "Nije uspjelo!",
                            Toast.LENGTH_LONG).show();
                } else {
                    map.addMarker(new MarkerOptions()
                            .position(new LatLng(lat, lon)));
                    CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(lat, lon)).zoom(14.0f).build();
                    CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
                    map.animateCamera(cameraUpdate);
                }

            }
        });

        mSwipeRefreshLayout.setRefreshing(false);
    }

    public Address getLocationFromAddress(String strAddress) {

        Geocoder coder = new Geocoder(getActivity());
        List<Address> address;
        Address location = null;

        try {
            address = coder.getFromLocationName(strAddress, 1);
            location = address.get(0);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return location;
    }
}

