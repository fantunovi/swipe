package com.example.frane.swipe;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by Frane on 16.3.2015..
 */
public class UnosPodatakaFragment extends Fragment {

    SQLiteHelper db;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_unos, container, false);

        final EditText editTextName = (EditText) rootView.findViewById(R.id.editTextName);
        final EditText editTextSurname = (EditText) rootView.findViewById(R.id.editTextSurname);
        final EditText editTextOIB = (EditText) rootView.findViewById(R.id.editTextOIB);
        final EditText editTextAddress = (EditText) rootView.findViewById(R.id.editTextAddress);
        final Button buttonAdd = (Button) rootView.findViewById(R.id.buttonAdd);


        db = new SQLiteHelper(getActivity());


        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = editTextName.getText().toString();
                String surname = editTextSurname.getText().toString();
                String oib = editTextOIB.getText().toString();
                String address = editTextAddress.getText().toString();
                db.dodajOsobu(new Osoba(name, surname, oib, address));

                Toast.makeText(getActivity(), "Osoba je dodana",
                        Toast.LENGTH_LONG).show();
            }
        });

        return rootView;
    }
}
