package com.example.frane.swipe;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * Created by Frane on 17.3.2015..
 */
public class AdministracijaPodatakaFragment extends Fragment {
    SQLiteHelper db;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_administracija, container, false);

        final Button delete = (Button) rootView.findViewById(R.id.buttonDelete);

        db = new SQLiteHelper(getActivity());

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db.izbrisiTablicu();
            }
        });


        return rootView;
    }


}