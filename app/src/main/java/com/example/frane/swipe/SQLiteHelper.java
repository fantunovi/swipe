package com.example.frane.swipe;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Frane on 16.3.2015..
 */
public class SQLiteHelper extends SQLiteOpenHelper {
    private static final int database_VERSION = 1;
    private static final String database_NAME = "Osobe.db";

    public SQLiteHelper(Context context) {
        super(context, database_NAME, null, database_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String NAPRAVI_TABLICU = "CREATE TABLE osobe (" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "ime TEXT, " +
                "prezime TEXT, " +
                "oib TEXT, " +
                "adresa TEXT" +
                " )";
        db.execSQL(NAPRAVI_TABLICU);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS osobe");
        onCreate(db);
    }

    public void dodajOsobu(Osoba osoba) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("ime", osoba.getName());
        values.put("prezime", osoba.getSurname());
        values.put("oib", osoba.getOib());
        values.put("adresa", osoba.getAddress());

        db.insert("osobe", null, values);

        db.close();
    }

    public List<Osoba> izlistajOsobe(){
        List<Osoba> list = new ArrayList<>();

        String query = "SELECT * FROM osobe";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        Osoba osoba = null;
        if (cursor.moveToFirst()){
            do {
                osoba = new Osoba();
                osoba.setId(Integer.parseInt(cursor.getString(0)));
                osoba.setName(cursor.getString(1));
                osoba.setSurname(cursor.getString(2));
                osoba.setOib(cursor.getString(3));
                osoba.setAddress(cursor.getString(4));

                list.add(osoba);
            } while (cursor.moveToNext());
        }
        return list;
    }

    public void izbrisiTablicu(){
        SQLiteDatabase db = this.getWritableDatabase();

        db.execSQL("DELETE FROM osobe");

        return;
    }

}