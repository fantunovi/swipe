package com.example.frane.swipe;

/**
 * Created by Frane on 16.3.2015..
 */
public class Osoba {

    private int id;
    private String name;
    private String surname;
    private String oib;
    private String address;

    public Osoba(String name, String surname, String oib, String address) {
        super();
        this.name = name;
        this.surname = surname;
        this.oib = oib;
        this.address = address;
    }

    public Osoba() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getOib() {
        return oib;
    }

    public void setOib(String oib) {
        this.oib = oib;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}